from app.routers.borrows_routers import *
from app.routers.customers_routers import *
from flask import Flask
from config import Config
from flask_jwt_extended import JWTM

app = Flask(__name__)
app.config.from_object(Config)
jwt = JWTManager(app)


app.register_blueprint(customers_blueprint)
app.register_blueprint(borrow_blueprint)
