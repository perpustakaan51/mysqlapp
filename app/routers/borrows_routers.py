from app import app
from app.controller import borrows_controllers
from flask import Blueprint, request

borrow_blueprint = Blueprint('borrows_routers', __name__)


@app.route('/borrows', methods=['GET'])
def showBorrow():
    return borrows_controllers.show()


@app.route('/borrows/insert', methods=['POST'])
def insertBorrow():
    params = request.json
    return borrows_controllers.insert(**params)


@app.route('/borrows/status', methods=['POST'])
def updateBorrow():
    params = request.json
    return borrows_controllers.changestatus(**params)
